using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using Snake.GameScreens;

namespace Snake.Components
{
    public class GameStateManager : Microsoft.Xna.Framework.GameComponent
    {
	Stack<GameScreen> stateStack = new Stack<GameScreen>();
	
        public GameStateManager(Game game)
            : base(game)
        {
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

	public GameScreen CurrentScreen {
		get { return stateStack.Peek(); }
	}

	public void PushState(GameScreen screen) {
		stateStack.Push(screen);
	}
	public void PopState() {
		stateStack.Pop();
	}
    }
}
