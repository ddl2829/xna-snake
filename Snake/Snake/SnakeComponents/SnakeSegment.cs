﻿using Microsoft.Xna.Framework;

namespace Snake.SnakeComponents
{
    public class SnakeSegment
    {
        public Vector2 Position;

        public SnakeSegment(Vector2 position)
        {
            this.Position = position;
        }
    }
}
