﻿using System;
using System.Collections.Generic;

using System.Diagnostics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Snake.GameScreens;
using Snake.Components;

namespace Snake.SnakeComponents
{
    public enum MoveDirections { UP, RIGHT, DOWN, LEFT };

    //Inherit from list so we dont have to manually track segments
    public class TheSnake : List<SnakeSegment>
    {
        //Events for the action screen to listen for to update score or change game state by
        public event EventHandler SelfCollision;
        public event EventHandler SnakeMove;

        //Snake Movement Speed
        TimeSpan MoveSpeed = TimeSpan.FromMilliseconds((double)100.0);
        TimeSpan MoveTimer = TimeSpan.Zero;

        //Snake Segment Texture
        Texture2D SegmentTexture;

        //Current and next directions to move
        MoveDirections nextMove = MoveDirections.LEFT;
        MoveDirections currentMove = MoveDirections.LEFT;

        //Position of the snake when initialized
        Vector2 InitialPosition;

        public TheSnake(Vector2 initialPosition, Texture2D segmentTexture) : base()
        {
            this.SegmentTexture = segmentTexture;
            InitialPosition = initialPosition;

            Debug.Print("Initial Snake Position: ({0}, {1})", initialPosition.X, initialPosition.Y);

            SetInitialSegments(initialPosition);
        }

        //Create the three snake segments at the given position
        private void SetInitialSegments(Vector2 initialPosition)
        {
            this.Add(new SnakeSegment(initialPosition));
            this.Add(new SnakeSegment(initialPosition + new Vector2(1, 0)));
            this.Add(new SnakeSegment(initialPosition + new Vector2(2, 0)));
        }

        public void Update(GameTime gameTime)
        {
            MoveTimer += gameTime.ElapsedGameTime;

            //Handle changing next move direction
            if (InputHandler.KeyPressed(Keys.Left) && currentMove != MoveDirections.RIGHT)
                nextMove = MoveDirections.LEFT;
            if (InputHandler.KeyPressed(Keys.Right) && currentMove != MoveDirections.LEFT)
                nextMove = MoveDirections.RIGHT;
            if (InputHandler.KeyPressed(Keys.Up) && currentMove != MoveDirections.DOWN)
                nextMove = MoveDirections.UP;
            if (InputHandler.KeyPressed(Keys.Down) && currentMove != MoveDirections.UP)
                nextMove = MoveDirections.DOWN;

            //Move if the timer is greater than the defined speed
            if (MoveTimer > MoveSpeed)
            {
                currentMove = nextMove;
                Move();
                MoveTimer = TimeSpan.Zero;
            }

            //Check if the head has hit any other segment and fire a self collision event if so
            for (int i = 1; i < this.Count; ++i)
            {
                if (this[0].Position == this[i].Position)
                    SelfCollision(this, null);
            }
        }

        //Move the snake by an amount (for title screen)
        public void Move(Vector2 movement)
        {
            //Loop through each segment setting it's position to the previous segment's position
            Vector2 followPosition = this[0].Position;
            for (int i = 0; i < this.Count; ++i)
            {
                if (i == 0)
                {
                    this[0].Position += movement;
                    continue;
                }
                Vector2 temp = this[i].Position;
                this[i].Position = followPosition;
                followPosition = temp;
            }
        }

        //Move the snake in the direction specified by the move direction (for game play)
        private void Move()
        {
            //Set the movement vector2 based on the nextMove direction
            Vector2 movement = Vector2.Zero;
            switch (nextMove)
            {
                case MoveDirections.LEFT:
                    movement.X = -1;
                    break;
                case MoveDirections.RIGHT:
                    movement.X = 1;
                    break;
                case MoveDirections.UP:
                    movement.Y = -1;
                    break;
                case MoveDirections.DOWN:
                    movement.Y = 1;
                    break;
            }
            //Loop through each segment setting it's position to the previous segment's position
            //Possible Refactor: Call the above move method with the calculated vector2 passed as the parameter
            Vector2 followPosition = this[0].Position;
            for (int i = 0; i < this.Count; ++i)
            {
                if (i == 0)
                {
                    this[0].Position += movement;
                    continue;
                }
                Vector2 temp = this[i].Position;
                this[i].Position = followPosition;
                followPosition = temp;
                SnakeMove(this, null);
            }
        }

        //Loop through and draw each snake segment
        public void Draw(SpriteBatch spriteBatch)
        {
            foreach(SnakeSegment segment in this) {
                spriteBatch.Draw(SegmentTexture, new Rectangle((int)segment.Position.X * ActionScreen.gridSpacing + 1, (int)segment.Position.Y * ActionScreen.gridSpacing + 1, ActionScreen.gridSpacing - 1, ActionScreen.gridSpacing - 1), Color.White);
            }
        }

        //Add a snake segment
        internal void AddSegment()
        {
            //Subtract the next to last piece's position from the last piece's position to get a new end position
            Vector2 relativePosition = this[this.Count - 1].Position - this[this.Count - 2].Position;
            //Add a new segment at that position
            this.Add(new SnakeSegment(this[this.Count - 1].Position + relativePosition));
        }

        //Reset the snake object to it's intial position
        internal void Reset()
        {
            this.nextMove = MoveDirections.LEFT;
            this.currentMove = MoveDirections.LEFT;
            this.Clear();
            this.SetInitialSegments(InitialPosition);
        }
    }
}
