//This class is heavily inspired by the XNA RPG Tutorials by Jamie McMahon at XNAGPA.net
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Snake.Components
{
    //Manages the state of the game
    public class GameStateManager : Microsoft.Xna.Framework.GameComponent
    {
        //State change event other classes can listen for to do actions such as cleanup or spawning objects based on state
        public event EventHandler OnStateChange;

        Stack<GameState> gameStates = new Stack<GameState>();

        //DrawableGameComponents have a DrawOrder, so we could keep the game state enabled and push a menu state to draw both
        const int startDrawOrder = 5000;
        const int drawOrderInc = 100;
        int drawOrder;

        public GameStateManager(Game game)
            : base(game)
        {
            drawOrder = startDrawOrder;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public GameState CurrentState
        {
            get { return gameStates.Peek(); }
        }

        public void PushState(GameState newState)
        {
            drawOrder += drawOrderInc;
            newState.DrawOrder = drawOrder;
            AddState(newState);
            if (OnStateChange != null)
                OnStateChange(this, null);
        }

        public void PopState()
        {
            if (gameStates.Count > 0)
            {
                RemoveState();
                drawOrder -= drawOrderInc;
                if (OnStateChange != null)
                    OnStateChange(this, null);
            }
        }
        private void RemoveState()
        {
            GameState State = gameStates.Peek();
            OnStateChange -= State.StateChange;
            Game.Components.Remove(State);
            gameStates.Pop();
        }

        private void AddState(GameState newState)
        {
            gameStates.Push(newState);
            Game.Components.Add(newState);
            OnStateChange += newState.StateChange;
        }

        public void ChangeState(GameState newState)
        {
            while (gameStates.Count > 0)
                RemoveState();
            newState.DrawOrder = startDrawOrder;
            drawOrder = startDrawOrder;
            AddState(newState);
            if (OnStateChange != null)
                OnStateChange(this, null);
        }
    }
}
