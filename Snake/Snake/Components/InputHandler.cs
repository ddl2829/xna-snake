using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Snake.Components
{
    //The inputhandler contains static methods to check keyboard, gamepad, and mouse input
    public class InputHandler : Microsoft.Xna.Framework.GameComponent
    {
        static KeyboardState keyboardState;
        static KeyboardState lastKeyboardState;

        static MouseState mouseState;
        static MouseState lastMouseState;

        public static KeyboardState KeyboardState
        {
            get { return keyboardState; }
        }

        public static KeyboardState LastKeyboardState
        {
            get { return lastKeyboardState; }
        }

        public InputHandler(Game game)
            : base(game)
        {
            mouseState = Mouse.GetState();
            keyboardState = Keyboard.GetState();
        }

        public override void Initialize()
        {
            base.Initialize();
        }


        //Updates the states of the various game input devices
        public override void Update(GameTime gameTime)
        {
            lastKeyboardState = keyboardState;
            keyboardState = Keyboard.GetState();

            lastMouseState = mouseState;
            mouseState = Mouse.GetState();
            
            base.Update(gameTime);
        }

        public static void Flush()
        {
            lastKeyboardState = keyboardState;
            lastMouseState = mouseState;
        }

        //Returns true if a key was just released
        public static bool KeyReleased(Keys key)
        {
            return keyboardState.IsKeyUp(key) && lastKeyboardState.IsKeyDown(key);
        }

        //Returns true if a key was just pressed
        public static bool KeyPressed(Keys key)
        {
            return keyboardState.IsKeyDown(key) && lastKeyboardState.IsKeyUp(key);
        }

        //Returns true if a key is being held down
        public static bool KeyDown(Keys key)
        {
            return keyboardState.IsKeyDown(key);
        }

        //Returns true if the left mouse button was clicked
        public static bool MouseLeftClicked()
        {
            return (mouseState.LeftButton == ButtonState.Pressed) && (lastMouseState.LeftButton == ButtonState.Released);
        }

        //Returns a vector2 containing the mouse position
        public static Vector2 MousePosition()
        {
            return new Vector2(mouseState.X, mouseState.Y);
        }
    }
}
