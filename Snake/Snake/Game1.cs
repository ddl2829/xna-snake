using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Snake.Components;
using Snake.GameScreens;

namespace Snake
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        public GraphicsDeviceManager Graphics;
        public SpriteBatch SpriteBatch;

        //State manager keeps track of which game screen we're on
        GameStateManager stateManager;

        //This is the rectangle of screen dimensions
        public readonly Rectangle ScreenRectangle;

        //Predefined screen dimensions
        const int screenWidth = 1200;
        const int screenHeight = 800;

        //Game Screens
        public TitleScreen TitleScreen;
        public ActionScreen ActionScreen;
        public GameOverScreen GameOverScreen;

        public Game1()
        {
            Graphics = new GraphicsDeviceManager(this);

            //Set the window to the predefined size
            Graphics.PreferredBackBufferWidth = screenWidth;
            Graphics.PreferredBackBufferHeight = screenHeight;
            //Initialize ScreenRectangle to that size for referencing later anywhere else in the program
            ScreenRectangle = new Rectangle(0, 0, screenWidth, screenHeight);

            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            //Initialize the input handler and register as a game component for easy input access anywhere
            Components.Add(new InputHandler(this));

            //Initialize and register the game state manager
            stateManager = new GameStateManager(this);
            Components.Add(stateManager);

            //Initialize each game screen
            TitleScreen = new TitleScreen(this, stateManager);
            ActionScreen = new ActionScreen(this, stateManager);
            GameOverScreen = new GameOverScreen(this, stateManager);

            //Set the current active state to the title screen
            stateManager.ChangeState(TitleScreen);

            base.Initialize();
        }

        protected override void LoadContent()
        {
            SpriteBatch = new SpriteBatch(GraphicsDevice);
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            //Override to exit the game at any time via the gamepad back or keyboard escape
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || InputHandler.KeyPressed(Keys.Escape))
                this.Exit();

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            //Clear background to a yellow-green color (kinda looks like a grassy field)
            GraphicsDevice.Clear(Color.YellowGreen);

            base.Draw(gameTime);
        }
    }
}
