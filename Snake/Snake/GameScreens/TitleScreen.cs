﻿using System;
using System.Diagnostics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using Snake.Components;
using Snake.Components.Controls;
using Snake.SnakeComponents;

namespace Snake.GameScreens
{
    public class TitleScreen : BaseGameScreen
    {
        LinkLabel startLabel;
        TheSnake theSnake;
        const int gridSpacing = 20;
        TimeSpan moveTimer = TimeSpan.FromMilliseconds((double)100.0);
        TimeSpan lastMove = TimeSpan.Zero;
        Vector2 snakeMaxDimensions;

        public TitleScreen(Game game, GameStateManager manager)
            : base(game, manager)
        {

        }

        public override void Initialize()
        {
            snakeMaxDimensions = new Vector2((int)GameRef.ScreenRectangle.Width / gridSpacing - 2, (int)GameRef.ScreenRectangle.Height / gridSpacing - 2);
            base.Initialize();
        }

        protected override void LoadContent()
        {
            ContentManager Content = GameRef.Content;
            base.LoadContent();

            Texture2D snakeSegment = Content.Load<Texture2D>(@"Sprites\SnakeSegment");

            theSnake = new TheSnake(new Vector2(0, 0), snakeSegment);

            //Set the start label properties (overkill in this project, but at least the framework exists for later use)
            startLabel = new LinkLabel();
            startLabel.Text = "Press ENTER to begin";
            startLabel.Position = new Vector2((GameRef.ScreenRectangle.Width - startLabel.SpriteFont.MeasureString(startLabel.Text).X) / 2, 600);
            
            startLabel.Color = Color.White;
            startLabel.SelectedColor = Color.White;
            startLabel.TabStop = true;
            startLabel.HasFocus = true;
            startLabel.Selected += new EventHandler(startLabel_Selected);
            ControlManager.Add(startLabel);
        }

        public override void Update(GameTime gameTime)
        {
            //Very inefficient last minute code for the titlescreen snake animation
            lastMove += gameTime.ElapsedGameTime;
            if (lastMove > moveTimer)
            {
                if (theSnake[0].Position.X == 0)
                {
                    if (theSnake[0].Position.Y == 0)
                        theSnake.AddSegment();
                    if (theSnake[0].Position.Y > snakeMaxDimensions.Y)
                    {
                        theSnake.Move(new Vector2(1, 0));
                    }
                    else
                    {
                        theSnake.Move(new Vector2(0, 1));
                    }
                }
                else if (theSnake[0].Position.Y >= snakeMaxDimensions.Y)
                {
                    if (theSnake[0].Position.X > snakeMaxDimensions.X)
                    {
                        theSnake.Move(new Vector2(0, -1));
                    }
                    else
                    {
                        theSnake.Move(new Vector2(1, 0));
                    }
                }
                else if (theSnake[0].Position.X >= snakeMaxDimensions.X)
                {
                    if (theSnake[0].Position.Y > 0)
                    {
                        theSnake.Move(new Vector2(0, -1));
                    }
                    else
                    {
                        theSnake.Move(new Vector2(-1, 0));
                    }
                }
                else if (theSnake[0].Position.Y == 0)
                {
                    theSnake.Move(new Vector2(-1, 0));
                }
                lastMove = TimeSpan.Zero;
            }

            //Update the control manager
            ControlManager.Update(gameTime);
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GameRef.SpriteBatch.Begin();

            base.Draw(gameTime);

            //Draw the title and controls
            string titleString = string.Format("CS485G-003 Snake");
            string creditString = string.Format("By Dale Smith");
            SpriteFont drawFont = startLabel.SpriteFont;
            GameRef.SpriteBatch.DrawString(drawFont, titleString, new Vector2((GameRef.ScreenRectangle.Width - drawFont.MeasureString(titleString).X) / 2, 100), Color.White);
            GameRef.SpriteBatch.DrawString(drawFont, creditString, new Vector2((GameRef.ScreenRectangle.Width - drawFont.MeasureString(creditString).X) / 2, 300), Color.White);
            ControlManager.Draw(GameRef.SpriteBatch);

            //Draw the animated snake
            theSnake.Draw(GameRef.SpriteBatch);

            GameRef.SpriteBatch.End();
        }

        private void startLabel_Selected(object sender, EventArgs e)
        {
            StateManager.PushState(GameRef.ActionScreen);
        }
    }
}
