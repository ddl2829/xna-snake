﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using Snake.Components;
using Snake.SnakeComponents;

namespace Snake.GameScreens
{
    public class ActionScreen : BaseGameScreen
    {
        //Texture for the food pick ups
        Texture2D foodItem;

        //Score spritefont
        SpriteFont scoreFont;

        //Grid basic effect, matrix camera, and horizontal line arrays
        BasicEffect gridEffect;
        Matrix Camera;
        VertexPositionColor[] horizontalLines;
        VertexPositionColor[] verticalLines;

        //Spacing for the grid
        public static int gridSpacing = 20;
        //Maximum amount of food to be spawned at once
        public const int maxFood = 15;
        //Rate the food spawns at
        TimeSpan FoodSpawnRate = TimeSpan.FromSeconds((double)2.0);
        TimeSpan lastFoodSpawn = TimeSpan.Zero;

        //List of positions the food is at, no reason to store objects holding the food's texture wasting memory
        public List<Vector2> foodPositions = new List<Vector2>();

        Random Rand = new Random();
        
        //Game Score and the Snake itself
        int Score = 0;
        TheSnake theSnake;

        public ActionScreen(Game1 game, GameStateManager manager)
            : base(game, manager)
        {
        }

        //Calculate the grid lines
        public override void Initialize()
        {
            //Initialize the vertexpositioncolor arrays with size depending on the screen width and grid spacing
            horizontalLines = new VertexPositionColor[(GameRef.ScreenRectangle.Height / gridSpacing)*2];
            verticalLines = new VertexPositionColor[(GameRef.ScreenRectangle.Width / gridSpacing)*2];

            //Loop through the horizontal lines, set the colors to white and the position varying by Y
            for (int i = 0, j = 1; i < horizontalLines.GetLength(0); i += 2, ++j)
            {
                horizontalLines[i].Position = new Vector3(0.0f, (float) j * gridSpacing, 1.0f);
                horizontalLines[i].Color = new Color(1.0f, 1.0f, 1.0f);

                horizontalLines[i+1].Position = new Vector3((float)GameRef.ScreenRectangle.Width, (float) j * gridSpacing, 1.0f);
                horizontalLines[i+1].Color = new Color(1.0f, 1.0f, 1.0f);
            }

            //Loop through the vertical lines, set the colors to white and the position varying by X
            for (int i = 0, j = 1; i < verticalLines.GetLength(0); i += 2, ++j)
            {
                verticalLines[i].Position = new Vector3((float)j * gridSpacing, 0.0f, 1.0f);
                verticalLines[i].Color = new Color(1.0f, 1.0f, 1.0f);

                verticalLines[i + 1].Position = new Vector3((float)j * gridSpacing, (float)GameRef.ScreenRectangle.Width, 1.0f);
                verticalLines[i + 1].Color = new Color(1.0f, 1.0f, 1.0f);
            }

            base.Initialize();
        }

        protected override void LoadContent()
        {
            ContentManager Content = GameRef.Content;

            //Load the food texture, snake segment texture, and score spritefont
            foodItem = Content.Load<Texture2D>(@"Sprites\Food");
            Texture2D snakeSegment = Content.Load<Texture2D>(@"Sprites\SnakeSegment");
            scoreFont = Content.Load<SpriteFont>(@"Fonts\ScoreFont");

            //Initialize the snake with the loaded texture and it's position centered on the screen
            theSnake = new TheSnake(new Vector2(verticalLines.Length / 4, horizontalLines.Length / 4), snakeSegment);

            //Register a self collision handler to end the game and a movement handler for scoring
            theSnake.SelfCollision += new EventHandler(theSnake_SelfCollision);
            theSnake.SnakeMove += new EventHandler(theSnake_SnakeMove);

            //Initialize the basic effect to draw the grid, set the matrix parameters to draw orthographically
            gridEffect = new BasicEffect(GraphicsDevice);
            Camera = Matrix.CreateOrthographicOffCenter(0.0f, (float)GameRef.ScreenRectangle.Width, (float)GameRef.ScreenRectangle.Height, 0.0f, 0.0f, -10.0f);

            gridEffect.Projection = Camera;
            gridEffect.View = Matrix.Identity;
            gridEffect.World = Matrix.Identity;
            gridEffect.VertexColorEnabled = true;
            
            RasterizerState rs = new RasterizerState();
            rs.CullMode = CullMode.None;
            GraphicsDevice.RasterizerState = rs;

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            //Spawn food every 2 seconds
            lastFoodSpawn += gameTime.ElapsedGameTime;
            if (lastFoodSpawn > FoodSpawnRate && foodPositions.Count < maxFood)
            {
                Vector2 t_newFoodPosition = new Vector2(Rand.Next(verticalLines.Length / 2), Rand.Next(horizontalLines.Length / 2));
                foodPositions.Add(t_newFoodPosition);
                Debug.Print("Food spawned at grid position ({0}, {1})", t_newFoodPosition.X, t_newFoodPosition.Y);
                lastFoodSpawn = TimeSpan.Zero;
            }

            //Update the snake
            theSnake.Update(gameTime);

            //Check for snake-food collisions
            for(int i = foodPositions.Count - 1; i >= 0; --i) 
            {
                if (foodPositions[i] == theSnake[0].Position)
                {
                    foodPositions.Remove(foodPositions[i]);
                    theSnake.AddSegment();
                    Score += 500;
                    break;
                }
            }

            //Check for the snake hitting the wall (could also be done with an event handler if the screen rectangle is passed to the snake)
            if (theSnake[0].Position.X < 0 || theSnake[0].Position.Y < 0 || theSnake[0].Position.X >= verticalLines.Length / 2 || theSnake[0].Position.Y >= horizontalLines.Length / 2)
            {
                GameRef.GameOverScreen.GameOverReason = false;
                GameRef.GameOverScreen.FinalScore = Score;
                StateManager.ChangeState(GameRef.GameOverScreen);
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GameRef.SpriteBatch.Begin();

            //Apply the orthographic view basic effect
            gridEffect.CurrentTechnique.Passes[0].Apply();

            //Draw the grid
            GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, horizontalLines, 0, horizontalLines.GetLength(0) / 2);
            GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, verticalLines, 0, verticalLines.GetLength(0) / 2);

            //Draw the snake
            theSnake.Draw(GameRef.SpriteBatch);

            //Draw each food object
            foreach (Vector2 foodPosition in foodPositions)
                GameRef.SpriteBatch.Draw(foodItem, new Rectangle((int)foodPosition.X * gridSpacing + 1, (int)foodPosition.Y * gridSpacing + 1, gridSpacing - 1, gridSpacing - 1), Color.White);

            //Draw the score
            string s_score = string.Format("Score: {0}", Score);
            GameRef.SpriteBatch.DrawString(scoreFont, s_score, new Vector2(0, (int)GameRef.ScreenRectangle.Height - scoreFont.MeasureString(s_score).Y), Color.White);

            base.Draw(gameTime);
            GameRef.SpriteBatch.End();
        }

        private void theSnake_SelfCollision(object sender, EventArgs e)
        {
            GameRef.GameOverScreen.GameOverReason = true;
            GameRef.GameOverScreen.FinalScore = Score;
            StateManager.ChangeState(GameRef.GameOverScreen);
        }

        private void theSnake_SnakeMove(object sender, EventArgs e)
        {
            Score -= 1;
        }

        public void ResetGame()
        {
            Score = 0;
            theSnake.Reset();
            lastFoodSpawn = TimeSpan.Zero;
            foodPositions.Clear();
        }
    }
}
