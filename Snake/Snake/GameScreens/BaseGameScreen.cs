//This class is heavily inspired by the XNA RPG Tutorials by Jamie McMahon at XNAGPA.net
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using Snake.Components;
using Snake.Components.Controls;

namespace Snake.GameScreens
{
    public abstract class BaseGameScreen : GameState
    {
        protected Game1 GameRef;
        protected ControlManager ControlManager;

        public BaseGameScreen(Game game, GameStateManager manager) : base(game, manager)
        {
            GameRef = (Game1)game;
        }

        protected override void LoadContent()
        {
            ContentManager Content = Game.Content;
            SpriteFont menuFont = Content.Load<SpriteFont>(@"Fonts\ControlFont");
            ControlManager = new ControlManager(menuFont);
            
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}
